/*
 * Copyright (c) 2017. by Zander Wong.
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 *
 */

package com.wander.test.kafka.configurer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.Properties;

public class Configurer {

    private static final Logger logger = LoggerFactory.getLogger(Configurer.class);
    private static Properties properties = new Properties();

    static {
        try {
            properties.load(Configurer.class.getResourceAsStream("/config.properties"));
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
    }

    public static String getString(String configFile, String key) {
        try {
            properties.load(Configurer.class.getClassLoader().getResourceAsStream(configFile));
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return properties.getProperty(key);
    }

    public static String getString(String key) {
        return properties.getProperty(key);
    }

    public static int getInteger(String configFile, String key) {
        try {
            properties.load(Configurer.class.getClassLoader().getResourceAsStream(configFile));
        } catch (IOException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
        }
        return Integer.parseInt(properties.getProperty(key));
    }

    public static int getInteger(String key) {
        return Integer.parseInt(properties.getProperty(key));
    }
}
