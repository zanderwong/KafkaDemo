package com.wander.test.kafka.utils;


import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AsyncProducerCallback implements Callback {

    private static final Logger logger = LoggerFactory.getLogger(AsyncProducerCallback.class);
    private static long offset;
    private static int partition;

    @Override
    public void onCompletion(RecordMetadata metadata, Exception exception) {
        setOffset(metadata.offset());
        setPartition(metadata.partition());
        logger.debug("sent metadata(offset:{}, partition:{})", offset, partition);
        if (exception != null) {
            //TODO: Handle the Exception of asyncProducer
            logger.error(exception.getMessage());
            exception.printStackTrace();
        }
    }

    public long getOffset() {
        return offset;
    }

    private void setOffset(long offset) {
        AsyncProducerCallback.offset = offset;
    }

    public int getPartition() {
        return partition;
    }

    private void setPartition(int partition) {
        AsyncProducerCallback.partition = partition;
    }


}
