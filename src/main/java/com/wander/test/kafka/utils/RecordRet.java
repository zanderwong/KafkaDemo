package com.wander.test.kafka.utils;

public class RecordRet {
    private final long offset;
    private final int partition;

    public RecordRet(long offset, int partition) {
        super();
        this.offset = offset;
        this.partition = partition;
    }

    public long getOffset() {
        return this.offset;
    }

    public int getPartition() {
        return this.partition;
    }

    @Override
    public String toString() {
        return "RecordRet{" +
                "offset=" + offset +
                ", partition=" + partition +
                '}';
    }
}
