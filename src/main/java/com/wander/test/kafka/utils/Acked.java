package com.wander.test.kafka.utils;

public class Acked {
    public static boolean ack = false;

    public Acked(boolean ack) {
        this.ack = ack;
    }

    public boolean isAck() {
        return ack;
    }
}
