/*
 * Copyright (c) 2017. by Zander Wong.
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 *
 */

package com.wander.test.kafka.comsume;


import com.wander.test.kafka.configurer.Configurer;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.ConsumerRecords;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.Arrays;
import java.util.Properties;

public class ConsumerTest {
    private static final Logger logger = LoggerFactory.getLogger(ConsumerTest.class);
    private static final String TOPIC = "test";

    public static void main(String[] args) {
        Properties props = new Properties();
        props.put("bootstrap.servers", Configurer.getString("kafka.bootstrap.servers"));
        props.put("group.id", "test");
        props.put("enable.auto.commit", "true");
        props.put("auto.commit.interval.ms", "1000");
        props.put("session.timeout.ms", "30000");
        props.put("key.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        props.put("value.deserializer", "org.apache.kafka.common.serialization.StringDeserializer");
        KafkaConsumer<String, String> consumer = new KafkaConsumer<>(props);
        consumer.subscribe(Arrays.asList(TOPIC));
        while (true) {
            ConsumerRecords<String, String> records = consumer.poll(100);
            for (ConsumerRecord<String, String> record : records) {
                logger.info("offset = {}, partition ={}, key = {}, value = {}", record.offset(), record.partition(), record.key(), record.value());
            }
        }
    }

}
