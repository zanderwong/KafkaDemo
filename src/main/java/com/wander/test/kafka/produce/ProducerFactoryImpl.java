package com.wander.test.kafka.produce;

import com.wander.test.kafka.configurer.Configurer;
import com.wander.test.kafka.utils.AsyncProducerCallback;
import com.wander.test.kafka.utils.RecordRet;
import org.apache.kafka.clients.producer.*;
import org.apache.kafka.common.Metric;
import org.apache.kafka.common.MetricName;
import org.apache.kafka.common.PartitionInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;
import java.util.concurrent.TimeUnit;

public class ProducerFactoryImpl implements ProducerFactory {

    private static final Logger logger = LoggerFactory.getLogger(ProducerFactory.class);
    private static final Properties props = new Properties();

    public static Producer<String, String> getProducer() {
        props.put("bootstrap.servers", Configurer.getString("kafka.bootstrap.servers"));
        props.put("acks", Configurer.getString("kafka.acks"));
        props.put("retries", Configurer.getInteger("kafka.retries"));
        props.put("batch.size", Configurer.getInteger("kafka.batch.size"));
        props.put("linger.ms", Configurer.getInteger("kafka.linger.ms"));
        props.put("buffer.memory", Configurer.getInteger("kafka.buffer.memory"));
        props.put("key.serializer", Configurer.getString("kafka.key.serializer"));
        props.put("value.serializer", Configurer.getString("kafka.value.serializer"));

        return new KafkaProducer<>(props);
    }

    @Override
    public void flush() {

    }

    @Override
    public List<PartitionInfo> partitionsFor(String topic) {
        //TODO come back here when upstream API settles
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public Map<MetricName, ? extends Metric> metrics() {
        //TODO come back here when upstream API settles
        throw new UnsupportedOperationException("not implemented");
    }

    @Override
    public void close() {

    }

    @Override
    public void close(long timeout, TimeUnit unit) {

    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord record) {
        return null;
    }

    @Override
    public Future<RecordMetadata> send(ProducerRecord record, Callback callback) {
        return null;
    }

    @Override
    public int produce(Producer<String, String> producer, String topic, String msgKey, String msgVal) {
        try {
            producer.send(new ProducerRecord<>(topic, msgKey, msgVal));
            logger.debug("sent record(key:{}, msg:{})", msgKey, msgVal);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return 1;
        }
        return 0;
    }

    @Override
    public RecordRet syncProduce(Producer<String, String> producer, String topic, String msgKey, String msgVal) {
        RecordMetadata metadata;
        try {
            metadata = producer.send(new ProducerRecord<>(topic, msgKey, msgVal)).get();
            logger.debug("sent record(key:{}, msg:{}), metadata(offset:{}, partition:{})", msgKey, msgVal, metadata.offset(), metadata.partition());
        } catch (InterruptedException | ExecutionException | NullPointerException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }

        return new RecordRet(metadata.offset(), metadata.partition());
    }

    @Override
    public RecordRet syncPartionedProduce(Producer<String, String> producer, String topic, String msgKey, String msgVal, Integer partition) {
        RecordMetadata metadata;
        try {
            metadata = producer.send(new ProducerRecord<>(topic, partition, msgKey, msgVal)).get();
            System.out.printf("sent record(key:%s, msg:%s), metadata(offset:%d, partition:%d)\n", msgKey, msgVal, metadata.offset(), metadata.partition());
            logger.debug("sent record(key:{}, msg:{}), metadata(offset:{}, partition:{})", msgKey, msgVal, metadata.offset(), metadata.partition());
        } catch (InterruptedException | ExecutionException e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }

        return new RecordRet(metadata.offset(), metadata.partition());
    }

    @Override
    public RecordRet asyncProduce(Producer<String, String> producer, String topic, String msgKey, String msgVal) {
        AsyncProducerCallback asyncProducerCallback = new AsyncProducerCallback();

        try {
            producer.send(new ProducerRecord<>(topic, msgKey, msgVal), asyncProducerCallback);
        } catch (Exception e) {
            logger.error(e.getMessage());
            e.printStackTrace();
            return null;
        }

        return asyncProducerCallback.getOffset() != 0 ? new RecordRet(asyncProducerCallback.getOffset(), asyncProducerCallback.getPartition()) : null;
    }
}
