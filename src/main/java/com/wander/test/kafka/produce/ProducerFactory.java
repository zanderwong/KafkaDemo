/*
 * Copyright (c) 2017. by Zander Wong.
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 *
 */

package com.wander.test.kafka.produce;

import com.wander.test.kafka.utils.RecordRet;
import org.apache.kafka.clients.producer.Callback;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import java.util.concurrent.Future;

public interface ProducerFactory extends Producer{
    @Override
    void flush();

    @Override
    void close();

    @Override
    Future<RecordMetadata> send(ProducerRecord record);

    @Override
    Future<RecordMetadata> send(ProducerRecord record, Callback callback);

    int produce(Producer<String, String> producer, String topic, String msgKey, String msgVal);

    RecordRet syncProduce(Producer<String, String> producer, String topic, String msgKey, String msgVal);

    RecordRet syncPartionedProduce(Producer<String, String> producer, String topic, String msgKey, String msgVal, Integer partition);

    RecordRet asyncProduce(Producer<String, String> producer, String topic, final String msgKey, final String msgVal);

}