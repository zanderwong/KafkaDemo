/*
 * Copyright (c) 2017. by Zander Wong.
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 *
 */

package com.wander.test.kafka.configurer;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class ConfigurerTest {

    @BeforeEach
    void setUp() {
        System.out.println("@BeforeEach");
    }


    @Test
    void getString() {
        assertEquals("test", Configurer.getString("kafka.topic"));
    }

    @Test
    void getString1() {
        assertEquals(Byte.parseByte("test"), Byte.parseByte(Configurer.getString("/config.properties", "kafka.topic")));
    }

    @Test
    void getInteger() {
        assertEquals(0, Configurer.getInteger("kafka.retries"));
    }

    @Test
    void getInteger1() {
        assertEquals(0, Configurer.getInteger("/config.properties", "kafka.retries"));
    }

    @AfterEach
    void tearDown() {
        System.out.println("@AfterEach");
    }

}