/*
 * Copyright (c) 2017. by Zander Wong.
 * Lorem ipsum dolor sit amet, consectetur adipiscing elit.
 *
 */

package com.wander.test.kafka.produce;

import com.wander.test.kafka.configurer.Configurer;
import org.apache.kafka.clients.producer.Producer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ProducerFactoryTest {

    private static Producer<String, String> producer = ProducerFactoryImpl.getProducer();
    private ProducerFactoryImpl producerFactory = new ProducerFactoryImpl();

    @BeforeEach
    void setUp() {
        System.out.println("@BeforeEach executed");
    }

    @Test
    void testProduce() {
        Assertions.assertEquals(3, producerFactory.syncProduce(producer, "test", "1", "msg").getPartition());
    }

    @Test
    void testPartitionedProducer() {
        Assertions.assertEquals(5, producerFactory.syncPartionedProduce(producer, Configurer.getString("kafka.topic"), "", "", 5).getPartition());
    }

    @Test
    void testAsyncProducer() {
//        Assertions.assertEquals(3, producerFactory.asyncProduce(producer, "test", "1", "msg").getPartition());
    }

    @AfterEach
    void tearDown() {
        System.out.println("@AfterEach executed");
    }

}