package com.wander.test.kafka.produce;

import com.wander.test.kafka.configurer.Configurer;
import org.apache.kafka.clients.producer.Producer;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class ProducerFactoryImplTest {

    private static String msg = "{\"id\":0,\"name\":\"java\",\"language\":\"static\",\"birth\":1995,\"concurrency\":{\"category\":\"threads\",\"nonBlocking\":true,\"easyOfUse\":\"RequireCallbacks\"}}";


    private ProducerFactoryImpl producerFactory = new ProducerFactoryImpl();
    private static Producer<String, String> producer = ProducerFactoryImpl.getProducer();

    @BeforeEach
    void setUp() {
        System.out.println("@BeforeEach executed");
    }

    @AfterEach
    void tearDown() {
        System.out.println("@AfterEach executed");
    }

    @Test
    void getProducer() {
    }

    @Test
    void flush() {
    }

    @Test
    void partitionsFor() {
    }

    @Test
    void metrics() {
    }

    @Test
    void close() {
    }

    @Test
    void send() {
    }

    @Test
    void produce() {
        Assertions.assertEquals(0, producerFactory.produce(producer, "test", "key", msg));
    }

    @Test
    void syncProduce() {
        String syncMsg = "{\"id\":3,\"name\":\"javaScript\",\"language\":\"dynamic\",\"birth\":1995,\"concurrency\":{\"category\":\"threads\",\"nonBlocking\":true,\"easyOfUse\":\"RequireCallbacks\"}}";

        Assertions.assertEquals(1, producerFactory.syncProduce(producer, "test", "key", syncMsg).getPartition());
    }

    @Test
    void syncPartionedProduce() {
        String syncPartionedMsg = "{\"id\":2,\"name\":\"php\",\"language\":\"dynamic\",\"birth\":1995,\"concurrency\":{\"category\":\"processes\",\"nonBlocking\":false,\"easyOfUse\":\"\"}}";
        Assertions.assertEquals(2, producerFactory.syncPartionedProduce(producer, Configurer.getString("kafka.topic"), "key", syncPartionedMsg, 2).getPartition());
    }

    @Test
    void asyncProduce() {
        String asyncMsg = "{\"id\":1,\"name\":\"go\",\"language\":\"static\",\"birth\":2011,\"concurrency\":{\"category\":\"threads\",\"nonBlocking\":true,\"easyOfUse\":\"NoCallbacksNeeded\"}}";
        Assertions.assertEquals(null, producerFactory.asyncProduce(producer, "test", "key", asyncMsg).getPartition());
    }
}